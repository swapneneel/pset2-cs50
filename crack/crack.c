#include <cs50.h>
#include <stdio.h>
#include <stdlib.h>  //consists of the function exit()
#include <string.h>
#include <ctype.h>
#include <crypt.h>  //for the crypt function

//these 2 lines below are for using linux's crypt function
#define _XOPEN_SOURCE
#include <unistd.h>

//function definition
void crack(string k_final, char salt[]);

int main(int argc, string argv[])
{
    //for no argument
    if (argc <= 1)
    {
        printf("Useage: %s hash\n", argv[0]);
        return 1;
    }
    else if (argc > 2)  //for  more than one entry
    {
        printf("You are not allowed to enter more than one argument.\n");
        printf("Useage: %s hash\n", argv[0]);
        return 1;  //return 1 for an error as specs
    }

    string k_final, k = argv[1];
    //checking if the key is alnum or not
    for (int i = 0, n = strlen(argv[1]); i < n; i++)
    {
        if (isalnum(k[i]) || ispunct(k[i]))
        {
            if (i + 1 == n)
            {
                k_final = argv[1];
            }
        }
        else
        {
            printf("The key you've entered is not valid.\nRetry with only alphanumeric entry.\n");
            return 1;
        }
    }
    printf("Checking");
    //finding salt
    char salt[2];
    for (int i = 0; i < 2; i++)
    {
        salt[i] = k_final[i];
    }

    // function call
    crack(k_final, salt);
    return 0;
}

//function utilisation crack
void crack(string k_final, char salt[])
{
    //now trying to guess the pass with the help of C's crypt function
    string g_hash = k_final, n_hash, password;
    string All = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./0123456789";
    int m =  strlen(All);
    char guess[20];

    for (int i = 1; i <= 5; i++)
    {
        if (i == 1)
        {
            // one charecter
            guess[1] = '\0';
            for (int j = 0; j < m; j++)
            {
                guess[0] = All[j];
                printf(".");
                n_hash = crypt(guess, salt);

                if (strcmp(g_hash, n_hash) == 0)
                {
                    password = guess;
                    printf("\nPassword: %s\n", password);
                    exit(0);
                }
            }
        }
        else if (i == 2)
        {
            //for two charecter
            guess[2] = '\0';
            for (int j = 0; j < m; j++)
            {
                guess[0] = All[j];
                printf(".");
                for (int k = 0; k < m; k++)
                {
                    guess[1] = All[k];

                    n_hash = crypt(guess, salt);

                    if (strcmp(g_hash, n_hash) == 0)
                    {
                        password = guess;
                        printf("\nPassword: %s\n", password);
                        exit(0);
                    }
                }
            }
        }
        else if (i == 3)
        {
            //for three charecter
            guess[3] = '\0';
            for (int j = 0; j < m; j++)
            {
                guess[0] = All[j];
                printf(".");
                for (int k = 0; k < m; k++)
                {
                    guess[1] = All[k];
                    for (int l = 0; l < m; l++)
                    {
                        guess[2] = All[l];

                        n_hash = crypt(guess, salt);

                        if (strcmp(g_hash, n_hash) == 0)
                        {
                            password = guess;
                            printf("\nPassword: %s\n", password);
                            exit(0);
                        }
                    }
                }
            }
        }
        else if (i == 4)
        {
            //for four charecter
            guess[4] = '\0';
            for (int j = 0; j < m; j++)
            {
                guess[0] = All[j];
                printf(".");
                for (int k = 0; k < m; k++)
                {
                    guess[1] = All[k];
                    for (int l = 0; l < m; l++)
                    {
                        guess[2] = All[l];
                        for (int p = 0; p < m; p++)
                        {
                            guess[3] = All[p];

                            n_hash = crypt(guess, salt);

                            if (strcmp(g_hash, n_hash) == 0)
                            {
                                password = guess;
                                printf("\nPassword: %s\n", password);
                                exit(0);  //this is the function to end the entire program
                            }
                        }
                    }
                }
            }
        }
        else if (i == 5)
        {
            //for five charecter
            guess[5] = '\0';
            for (int j = 0; j < m; j++)
            {
                guess[0] = All[j];
                printf(".");
                for (int k = 0; k < m; k++)
                {
                    guess[1] = All[k];
                    for (int l = 0; l < m; l++)
                    {
                        guess[2] = All[l];
                        for (int p = 0; p < m; p++)
                        {
                            guess[3] = All[p];
                            for (int q = 0; q < m; q++)
                            {
                                guess[4] = All[q];

                                n_hash = crypt(guess, salt);

                                if (strcmp(g_hash, n_hash) == 0)
                                {
                                    password = guess;
                                    printf("\nPassword: %s\n", password);
                                    exit(0);
                                }
                                else
                                {
                                    if (q + 1 == m)  //At the end of all the iterations of the fifth place
                                    {
                                        //if nothing maching found than
                                        password = "No password found.\n";
                                        printf("\n%s", password);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


}
