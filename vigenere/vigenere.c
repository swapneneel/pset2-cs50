#include <cs50.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

//introducing the function
char vigenere(char char_check, char k_pass);

//using command line arguments
int main(int argc, string argv[])
{
    string k_final;
    //handling no input
    if (argc <= 1)
    {
        printf("Usage: %s k\n", argv[0]);
        return 1;
    }
    else if (argc > 2)//handling more than one input
    {
        printf("Usage: %s k\n", argv[0]);
        //printf("Sorry! You aren't allowed more than one key.\n");
        return 1;
    }
    else //handling one string input
    {
        //checking if the key does have a digit in between
        for (int i = 0, n = strlen(argv[1]); i < n; i++)
        {
            if (isalpha((argv[1])[i]))  //cheking if it is a digit or not
            {
                if (i + 1 == n) //for last ltr of the key string
                {
                    k_final = argv[1]; // final key is set
                }
                else
                {
                    continue;
                }
            }
            else
            {
                printf("Usage: %s k\n", argv[0]);
                return 1;
                break;
            }

        }//Key checking is done and proceded for the next step
    }

    //now prompting for the plaintext
    string p = get_string("plaintext: ");

    //processing one char by one and printing the ciphertext one by one
    printf("ciphertext: ");

    char k_pass, final_cipher; //some variable declearation

    int j, m = strlen(k_final);
    for (int i = 0, k = 0, n = strlen(p); i < n; i++)
    {

        char char_plain = p[i];
        if (isalpha(char_plain)) //for every alpha pain text char
        {
            j = k % m; // the concept said of grouping used
            k++;

            //this condition will call key if ptext is letter
            k_pass = k_final[j];
            final_cipher = vigenere(char_plain, k_pass); //function call
            printf("%c", final_cipher);

        }
        else
        {
            //this is becouse other than alpha others doesn't use a key nither cipher
            final_cipher = char_plain;// or p[i]
            printf("%c", final_cipher);
        }


    }
    printf("\n");


    return 0;//as written on the document

}


//defining the function(pre introduced)
/***** Taking input of each and every text & key send back as enxiphered ******/
char vigenere(char char_check, char k_pass)
{
    char ciphertext;
    int k_index, p_index, alg;

    if (islower(k_pass))
    {
        k_index = k_pass % 'a';
    }
    else
    {
        k_index = k_pass % 'A';
    }
    if (islower(char_check))
    {
        p_index = char_check % 'a';
        alg = (p_index + k_index) % 26;
        ciphertext = alg + 'a';
    }
    else
    {
        p_index = char_check % 'A';
        alg = (p_index + k_index) % 26;
        ciphertext = alg + 'A';
    }

    return ciphertext;
}