#include <cs50.h>
#include <stdio.h>
#include <stdlib.h>//for the atoi() function
#include <string.h>//for strlen() function
#include <ctype.h> //for toupper() and tolower()




//intro to functions
char crypto(char char_check, int k);

int main(int argc, string argv[])
{
    //printf("Usage: %s k", argv[0]);
    //first tackling error valuse of key

    if (argc == 1)//when with no argument
    {
        printf("Usage: %s k\n", argv[0]);
        return 1;
    }
    else if (argc > 2)
    {
        printf("Sorry! You aren't allowed more than one key.\n");
    }
    else // when valid number of arguments are added
    {
        int k, k_chk = atoi(argv[1]);//converting string to integer

        if (k_chk <= 0) //checking for positive & non-zero key
        {
            printf("The entered key value isn't acceptable. Please retry.\n");
            printf("Hint: Try input a int > 0.\n");
        }
        else
        {
            k = k_chk;;//actual is equal to key

            string p = get_string("plaintext: "); //getting the plain text

            char char_check, final_cipher;
            printf("ciphertext: ");

            //sorting every char from the string of plaintext
            for (int i = 0, n = strlen(p); i < n; i++)
            {
                char_check = p[i];//string 1D array
                final_cipher = crypto(char_check, k);//function call with right arguments

                printf("%c", final_cipher); //printing the shiffer text final
            }
            printf("\n");
        }
    }
}




//this is the function which is crypting the input
char crypto(char char_check, int k)
{
    //some conditions to be fullfilled as wished in the docs
    char ciphertext;



    if (isalnum(char_check))  //alphanumeric is sorted
    {
        if (islower(char_check)) //now sorted the lower case
        {
            int alg = (char_check % 'a' + k) % 26;
            ciphertext = alg + 'a'; //letting the lower lower
        }
        else if (isupper(char_check)) //now sorting uppercase
        {
            int alg = (char_check % 'A' + k) % 26;
            ciphertext = alg + 'A';
        }
        else //now sorting digits
        {
            ciphertext = char_check;
        }
    }
    else if (ispunct(char_check))
    {
        ciphertext = char_check;
    }
    else if (isspace(char_check))
    {
        ciphertext = char_check;
    }
    else
    {
        ciphertext = char_check;
    }

    return ciphertext;
}
